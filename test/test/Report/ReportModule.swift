//
//  ReportModule.swift
//  test
//
//  This class is in charge of initializing and creating instances for this feature
//
//  Created by Julien Saito on 1/4/19.
//  Copyright (c) 2019 coinspect. All rights reserved.
//

class ReportModule {
    
    //the instances
    private let reportView: ReportView
    private let reportPresenter: ReportPresenter
    private let reportDelegate: ReportDelegate
    private let reportAdapter: ReportTableViewAdapter
    
    //Be careful in the order of the creation of instances
    init(reportView: ReportView) {
        self.reportView = reportView
        self.reportDelegate = ReportDelegateImpl()
        self.reportPresenter = ReportPresenterImpl(reportView: reportView, reportDelegate: reportDelegate)
        self.reportAdapter = ReportTableViewAdapter(reportPresenter: reportPresenter)
    }
    
    func provideReportPresenter() -> ReportPresenter {
       return reportPresenter
    }
    
    func provideReportAdapter() -> ReportTableViewAdapter {
        return reportAdapter
    }
}
