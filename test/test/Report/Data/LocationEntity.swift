//
//  DataModel.swift
//  test
//
//  Created by Julien Saito on 1/4/19.
//  Copyright © 2019 coinspect. All rights reserved.
//

import Foundation

class Location {
    let accuracy, altitude, latitude, longitude: Double
    let time: String
    
    init(accuracy: Double, altitude: Double, latitude: Double, longitude: Double, time: String) {
        self.accuracy = accuracy
        self.altitude = altitude
        self.latitude = latitude
        self.longitude = longitude
        self.time = time
    }
}
