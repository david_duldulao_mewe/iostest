//
//  ReportPresenterImpl.swift
//  test
//
//  Created by Julien Saito on 1/4/19.
//  Copyright (c) 2019 coinspect. All rights reserved.
//

class ReportPresenterImpl: ReportPresenter
{
    let reportView: ReportView
    let reportDelegate: ReportDelegate
    
    // MARK: lifecycle
    init(reportView: ReportView,
        reportDelegate: ReportDelegate) {
        self.reportView = reportView
        self.reportDelegate = reportDelegate
    }
    
    // MARK: - main presenter logic
    func loadData() {
        self.reportDelegate.loadReports(index: 0, count: 10, onResponse: self.onResponse, onError: self.onError)
    }
    
    func loadMoreData() {
        
    }
    
    func loadLocation(with id: String) {
        reportView.showErrorMessage(errorMessage: "Already Loading")
    }
    
    // MARK: - load Event handling
    func onResponse(reports: [ReportEntity]) {
        self.reports.append(contentsOf: reports)
        reportView.displayMessage(message: "reportEntity loaded \(reports.count)")
        self.reportView.hideLoading()
    }
    
    func onError(error: Error) {
        self.reportView.showErrorMessage(errorMessage: error.localizedDescription)
        self.reportView.hideLoading()
    }
    
    // MARK: - reports
    var reports: [ReportEntity] = []
    func getNumberOfReports() -> Int {
        return reports.count
    }
    
    func getReport(for index: Int) -> ReportEntity {
        return reports[index]
    }

}
