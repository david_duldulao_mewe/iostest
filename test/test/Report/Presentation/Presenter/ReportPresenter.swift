//
//  ReportPresenter.swift
//  test
//
//  This class holds the logic of the Report feature.
//
//  Created by Julien Saito on 1/4/19.
//  Copyright (c) 2019 coinspect. All rights reserved.
//

protocol ReportPresenter {
    func loadData()
    func loadMoreData()
    func loadLocation(with id: String)
    
    func getNumberOfReports() -> Int
    func getReport(for index: Int) -> ReportEntity
}
