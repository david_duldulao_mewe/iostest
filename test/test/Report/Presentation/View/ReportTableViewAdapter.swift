//
//  ReportTableViewAdapter.swift
//  test
//
//  Created by Julien Saito on 1/4/19.
//  Copyright © 2019 coinspect. All rights reserved.
//

import Foundation
import UIKit

class ReportTableViewAdapter: NSObject, UITableViewDelegate, UITableViewDataSource {

    let presenter: ReportPresenter

    init(reportPresenter: ReportPresenter) {
        self.presenter = reportPresenter
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 0) {
            return self.presenter.getNumberOfReports()
        } else {
            return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ReportCell = tableView.dequeueReusableCell(withIdentifier: "ReportCell", for: indexPath) as! ReportCell
        let report = self.presenter.getReport(for: indexPath.row)
        cell.bind(with: report, presenter: self.presenter)
        return cell
    }

}
