//
//  ReportDelegateImpl.swift
//  test
//
//  Created by Julien Saito on 1/4/19.
//  Copyright (c) 2019 coinspect. All rights reserved.
//

import Foundation

class ReportDelegateImpl : ReportDelegate {
    
    let totalCount: Int = 100
    var data:[ReportEntity] = []
    
    init() {
        for i in 0 ... totalCount {
            data.append(ReportEntity(batteryLevel: Double(i), charging: true, time: "1545026433723", location: nil))
        }
    }
    
    func loadReports(index: Int, count: Int, onResponse: ([ReportEntity]) -> Void, onError: (Error) -> Void) {
        onResponse(Array(data[index..<count]))
    }
    
    func loadLocation(with Id: String) {
        
    }
    
    
}
